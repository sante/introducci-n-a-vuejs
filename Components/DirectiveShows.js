let DirectiveShow = {
    template: `<div>
                    <h1 v-text='title'></h1>
                    <p v-html='message'></p>
                    
                </div>`,
    data() {
        return {
            title: 'Directivas de v-Show',
            message: '<b>Hola desde directiva v-Show</b>',
            
        }
    },
}