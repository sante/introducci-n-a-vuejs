Vue.component('vue-directives', {
    template: `<div>
                    <h1 v-text='title'></h1>
                    <p v-text='text'></p>
                    <a v-bind:href='link.href' :title='link.title' v-text= 'link.text'></a>
                    <directive-html></directive-html>
                    <DirectiveShow/>
                </div>`,
    data() {
        return {
            text: 'texto de prueba v-text',
            title: 'Directivas de VueJs',
            link: {
                text: 'VueJs home',
                href: 'https://vuejs.org/',
                title: 'Documentacion Vue js'
            }
        }
    },
    components: {
        'directive-html': DirectiveHtml, 
        DirectiveShow
    }
})